#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_flash.h"
#include "stm32f10x_rcc.h"
#include "misc.h"
#include <platform_config.h>
#include "usb_lib.h"
#include "hw_config.h"
#include "usb_pwr.h"
#include "mass_mal.h"

#define BUTTON_PIN	GPIO_Pin_1


void GoToUserApp(u32 addr)
{
	void (*GoToApp)(void);
	//__disable_irq();//запрещаем прерывани
	u32 appJumpAddress = *((volatile u32*)(addr + 4));
	GoToApp = (void (*)(void))appJumpAddress;
	SCB->VTOR = addr;
	__set_MSP(*((volatile u32*) addr)); //stack pointer (to RAM) for USER app in this address
	GoToApp();
}

int main(void)
{
	GPIO_InitTypeDef  g;

	/* Initialize Button input PB */
	// Enable PORTB Clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	g.GPIO_Pin = LEDR | LEDG | LEDB;
	g.GPIO_Speed = GPIO_Speed_2MHz;
	g.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(CTRL_PORT, &g);

	/* Configure the GPIO_BUTTON pin */
	g.GPIO_Pin = GPIO_Pin_13;
	g.GPIO_Mode = GPIO_Mode_IPU;
	g.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &g);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

	if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_13) == Bit_SET) {
		// Go To Application
		GPIO_DeInit(GPIOA);
		GPIO_DeInit(GPIOB);
		GoToUserApp(0x08005000);
	}
	else {

		GPIO_DeInit(GPIOA);
		GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, DISABLE);
		GPIO_SetBits(CTRL_PORT, LEDG );
		// Initialize Mass Storage
		Set_System();
		Set_USBClock();
		USB_Interrupts_Config();
		USB_Init();
		while (bDeviceState != CONFIGURED);

		while (1)
		{

		}
	}
}

void
__attribute__((noreturn))
assert_failed (uint8_t* file, uint32_t line)
{

  abort ();
  /* NOTREACHED */
}


